﻿using System;
using System.ComponentModel;
using FeatureThrottle.Service.Viewer.ThrottleService;
using FeatureThrottle.ServiceViewer.MenuSystem;

namespace FeatureThrottle.Service.Viewer
{
    public class Program
    {
        static void Main(string[] args)
        {
            new MenuDrivenApplication(
                "Throttle Service Viewer",
                RetrieveServiceStatus,
                RetrieveActiveTasks,
                StopService,
                StartService,
                DisplayTaskId,
                CallThrottle).Run();
        }
        
        static bool IsTheServiceStopped()
        {
            var client = new ThrottleServiceClient();
            try
            {
                client.Open();
                return client.IsThrottleStopped();
            }
            finally
            {
                client.Close();
            }
        }

        [Description("Retrieve service status")]
        static void RetrieveServiceStatus()
        {
            if (IsTheServiceStopped())
            {
                Console.WriteLine("The Service is currently stopped.");
                Console.WriteLine("");
            }
            else
            {
                Console.WriteLine("The Service is currently running.");
                Console.WriteLine("");
            }
        }

        [Description("Retrieve active tasks")]
        static void RetrieveActiveTasks()
        {
            var client = new ThrottleServiceClient();
            Console.WriteLine("Connecting ....");
            client.Open();
            var data = client.ActiveTasks();
            Console.WriteLine("Retrieving data ....");
            client.Close();
            Console.WriteLine();
            Console.WriteLine("Service Data retrieved at {0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToShortTimeString());
            Console.WriteLine();
            foreach (var task in data)
            {
                Console.WriteLine("--------------------------------------------------------------------");
                Console.WriteLine("[{0}]-{1}", task.TaskId, task.Name);
                Console.WriteLine("Scheduled Start-[{0}] End-[{1}] Actual-[{2}]", task.ScheduledStart, task.ScheduledEnd, (task.ActualEnd == DateTime.MinValue) ? string.Empty : task.ActualEnd.ToString());
                Console.WriteLine("Count Current-[{0}] Target-[{1}] Total-[{2}] Probability-[{3}]", task.Counter.CurrentCount, task.Counter.TargetNumber, task.CallCount, task.Probability);
                Console.WriteLine("--------------------------------------------------------------------");
            }            
        }

        [Description("Stop throttle service")]
        static void StopService()
        {
            var client = new ThrottleServiceClient();
            try
            {
                Console.WriteLine("Connecting ....");
                client.Open();
                Console.WriteLine("Sending stop command to throttle");

                if (client.StopService())
                {
                    Console.WriteLine("Service stopped.");
                }
            }
            finally
            {
                client = null;
            }
        }

        [Description("Start throttle service")]
        static void StartService()
        {
            var client = new ThrottleServiceClient();
            Console.WriteLine("Connecting ....");
            client.Open();
            Console.WriteLine("Sending start command to throttle");

            if (client.StartService())
            {
                Console.WriteLine("Service started.");
            }
        }

        static void ResetTaskCounters()
        {
            Console.WriteLine("WARNING : This option will reset your task counter state.");
            Console.WriteLine();
            Console.WriteLine("Do you wish to continue : Y / N ?");

            var str = Console.ReadLine();
            if(!string.IsNullOrEmpty(str))
            {
                switch (str.ToLower())
                {
                    case "y":
                        ActionTaskReset();
                        break;
                    default:
                        Console.WriteLine("You have selected not to continue with the reset.");
                        break;
                }
            }
        }

        [Description("Call throttle")]
        static void CallThrottle()
        {
            bool result = false;

            do
            {
                Console.WriteLine("TASK ID : Please enter a valid task id.");
                Console.WriteLine();

                var str = Console.ReadLine();
                result = int.TryParse(str, out _taskId);
            } while (!result);

            ActionCallThrottle();
        }

        private static int _taskId;
        private static int _currentCount;

        [Description("Reset task counter (must be greater than current count)")]
        static void DisplayTaskId()
        {
            Console.WriteLine("TASK ID : Please enter the task id.");
            Console.WriteLine();

            var str = Console.ReadLine();
            if (int.TryParse(str, out _taskId))
                DisplayCounter();
            else 
                DisplayTaskId();
        }

        static void DisplayCounter()
        {
            Console.WriteLine("COUNTER : Please enter the new counter value.");
            Console.WriteLine();

            var str = Console.ReadLine();
            if (int.TryParse(str, out _currentCount))
                ResetTaskCounters();                
            else
                DisplayCounter();
        }

        private static void ActionTaskReset()
        {
            var client = new ThrottleServiceClient();
            Console.WriteLine("Connecting ....");
            client.Open();
            Console.WriteLine("Sending Reset command to throttle");
            if (client.ResetTaskCounter(_taskId, _currentCount))
            {
                Console.WriteLine("The counter for task id [{0}] has been reset to {1}.", _taskId, _currentCount);
            }
        }

        private static void ActionCallThrottle()
        {
            var client = new ThrottleServiceClient();
            Console.WriteLine("Connecting ....");
            client.Open();
            Console.WriteLine("Sending Call Throttle command to service");
            var response = client.CallThrottle(new ThrottleRequest() {TaskId = _taskId});

            if (response != null)
            {
                Console.WriteLine("The result for task with id={0} is: ", _taskId);
                Console.WriteLine("Outcome is: {0}", response.Outcome);
                Console.WriteLine("State is: {0}", response.State);
            }
            else
                Console.WriteLine("The response was null...");

            Console.WriteLine();
        }
    }
}