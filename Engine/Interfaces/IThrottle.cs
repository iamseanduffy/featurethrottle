﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Engine.Contracts;

namespace FeatureThrottle.Engine.Interfaces
{
    [ContractClass(typeof (ThrottleContract))]
    public interface IThrottle
    {
        List<Task> Tasks { get; }
        bool IsThrottleStopped { get; set; }
        ThrottleResponse GetResponse(ThrottleRequest throttleRequest);
        bool ResetTaskCounter(int taskId, int currentCount);
    }
}