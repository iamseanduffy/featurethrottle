using System.Diagnostics.Contracts;
using FeatureThrottle.Engine.Contracts;

namespace FeatureThrottle.Engine.Interfaces
{
    [ContractClass(typeof(NumberGeneratorContract))]
    public interface INumberGenerator
    {
        bool IsProbable(int probability);
    }
}
