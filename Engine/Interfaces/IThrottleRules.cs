﻿using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Domain.Types;
using FeatureThrottle.Engine.Contracts;


namespace FeatureThrottle.Engine.Interfaces
{
    [ContractClass(typeof(ThrottleRulesContract))]
    public interface IThrottleRules
    {
        event CompletedEventHandler TaskCompletedEvent;
        bool ApplyRules(Task task);
    }
}
