﻿using System.Diagnostics.Contracts;
using FeatureThrottle.Engine.Contracts;

namespace FeatureThrottle.Engine.Interfaces
{
    [ContractClass(typeof(ControllerContractcs))]
    public interface IController
    {
        IThrottle Throttle { get; set; }
        bool StopThrottle();
        bool StartThrottle();
    }
}
