﻿using System.Collections.Generic;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Engine.Business;

namespace FeatureThrottle.Engine.Interfaces
{
    public interface ITaskService
    {
        void UpdateTask(object sender, TaskArgs e);
        List<Task> LoadTasks { get; }
        event TaskService.ChangedEventHandler TaskChangedEvent;
        Task LoadTask(int taskId);
    }
}
