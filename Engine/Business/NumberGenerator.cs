﻿using System;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Business
{
    public sealed class NumberGenerator : INumberGenerator
    {
        internal const int MinNumber = 0;
        internal const int MaxNumber = 100;

        public bool IsProbable(int probability)
        {
            if (probability == 0)
            {
                return false;
            }

            var random = new Random();
            var myRandomNumber = random.Next(MinNumber, MaxNumber);
            return probability >= myRandomNumber;
        }
    }
}
