﻿using System;
using System.Configuration.Provider;
using FeatureThrottle.Data.Provider;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Business
{
    public sealed class Controller : IController, IDisposable
    {
        public IThrottle Throttle { get; set; }
        private Guid ManagerId { get; set; }
     
        internal Controller()
        {
            ManagerId = Guid.NewGuid();
            Initialise();
        }

        private void Initialise()
        {
            var configuration = ConfigurationBuilder.LoadConfiguration();

            if (configuration.StopThrottle)
            {
                throw new ProviderException("Throttle has been stopped.");
            }

            Throttle = new Throttle(new TaskService(new TaskRepository()), configuration.StopThrottle);
        }

        public bool StopThrottle()
        {
            if (Throttle != null)
            {
                Throttle.IsThrottleStopped = true;
                return true;
            }

            return false;
        }

        public bool StartThrottle()
        {
            if (Throttle != null)
            {
                Throttle.IsThrottleStopped = false;
                return true;
            }

            return false;
        }

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources, unregister events.
                if (Throttle != null)
                {
                    //if (Throttle.ThrottleRules != null) Throttle.ThrottleRules.TaskCompletedEvent -= CatchTaskCompletionEvent;
                    ((Throttle)Throttle).Dispose();
                    Throttle = null;
                }
            }
        }

        #endregion
    }
}
