﻿namespace FeatureThrottle.Engine.Business
{
    public static class ControllerFactory
    {
        private static Controller _instance;

        public static Controller Create()
        {
            return _instance ?? (_instance = new Controller());
        }
    }
}
