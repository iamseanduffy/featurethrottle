﻿using System;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Domain.Types;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Business
{
    public sealed class ThrottleRules : IThrottleRules
    {
        internal delegate bool BusinessRule(Task task);

        public event CompletedEventHandler TaskCompletedEvent;

        private void OnTaskComplete(TaskArgs e)
        {
            var handler = TaskCompletedEvent;
            if (handler != null) handler(this, e);
        }

        public ThrottleRules()
        {
            SetupBusinessRules();
        }

        private BusinessRule SetupBusinessRules()
        {
            var rules = new BusinessRule(AvailableNumbers);
            rules += CheckWorkDateTime;
            return rules;
        }

        // Returning true indicates all tasks have passed.
        public bool ApplyRules(Task task)
        {
            var rules = SetupBusinessRules();
            foreach (BusinessRule rule in rules.GetInvocationList())
            {
                var complete = rule.Invoke(task);
                if (!complete) return false;
                break;
            }
            return true;
        }

        // Return value of true indicates there are available numbers to allocate
        internal bool AvailableNumbers(Task task)
        {
           //<pex>
           Contract.Requires<ArgumentNullException>(task != null,"The task object cannot be null.");
           Contract.Requires<NullReferenceException>(task.Counter != null, "The counter cannot be null.");           
           Contract.Requires<ArgumentOutOfRangeException>(task.Counter.CurrentCount <= task.Counter.TargetNumber, "The current count cannot be greater than the Target Number!");
           //</pex>

           if (task.Counter.CurrentCount < task.Counter.TargetNumber) return true;
           OnTaskComplete(new TaskArgs{CurrentTaskCount = task.Counter.CurrentCount,TaskEndTime = DateTime.Now,TaskId = task.TaskId});
           return false;
        }

        // Return value of true indicates that the the Workunit is within its scheduled running time
        internal static bool CheckWorkDateTime(Task task)
        {
            //<pex>
            Contract.Requires<ArgumentNullException>(task != null, "The task object cannot be null.");
            Contract.Requires<ArgumentOutOfRangeException>(task.ScheduledEnd > task.ScheduledStart,"The ScheduledEnd time For a task cannot be before its ScheduledStart time.");
            //</pex>
            return task.ScheduledStart < DateTime.Now && task.ScheduledEnd > DateTime.Now;
        }
    }
}
