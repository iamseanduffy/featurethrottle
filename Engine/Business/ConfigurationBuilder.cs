﻿using System;
using System.Configuration;
using System.Diagnostics.Contracts;
using Configuration = FeatureThrottle.Domain.Entities.Configuration;

namespace FeatureThrottle.Engine.Business
{
    public sealed class ConfigurationBuilder
    {
        public static Configuration LoadConfiguration()
        {
            return new Configuration
            {
                StopThrottle = bool.Parse(GetValueByKey("StopThrottle")),
                TaskLoadInterval = int.Parse(GetValueByKey("TaskLoadInterval"))
            };
        }

        public static string GetValueByKey(string key)
        {
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(key), "Configuration value is NULL or empty.");

            return ConfigurationManager.AppSettings[key];
        } 
    }
}