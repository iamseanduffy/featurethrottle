﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Domain.Types;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Business
{
    public sealed class Throttle : IThrottle, IDisposable
    {
        private static readonly object Counterlock = new object();
        private static readonly object TaskCounterlock = new object();
        private INumberGenerator _numberGenerator { get; set; }
        private ITaskService _taskService { get; set; }
        private IThrottleRules _throttleRules { get; set; }
        public bool IsThrottleStopped { get; set; }
        
        public Throttle(ITaskService taskService, 
            bool isThrottleStopped)
        {
            _taskService = taskService;
            IsThrottleStopped = isThrottleStopped;
            _throttleRules = new ThrottleRules();
            _numberGenerator = new NumberGenerator();
        }

        public List<Task> Tasks
        {
            get { return _taskService.LoadTasks; }
        }

        public ThrottleResponse GetResponse(ThrottleRequest throttleRequest)
        {
            var task = _taskService.LoadTask(throttleRequest.TaskId);

            if (task == null)
            {
                return new ThrottleResponse
                {
                    Outcome = OutcomeText.TaskNotFound,
                    State = false
                };
            }

            AddtoRequestCounter(task);

            if (IsThrottleStopped)
            {
                return new ThrottleResponse
                {
                    Outcome = OutcomeText.ThrottleStopped,
                    State = false
                };
            }

            if (_numberGenerator.IsProbable(task.Probability) == false)
            {
                return new ThrottleResponse
                {
                    Outcome = OutcomeText.Decline,
                    State = false
                };
            }

            lock (Counterlock)
            {
                if (_throttleRules.ApplyRules(task) == false)
                {
                    return new ThrottleResponse
                    {
                        Outcome = OutcomeText.RulesDecline,
                        State = false
                    };
                }

                task.Counter.Add();
                return new ThrottleResponse
                {
                    Outcome = OutcomeText.Accept,
                    State = true
                };
            }
        }

        private void AddtoRequestCounter(Task task)
        {
            Contract.Requires<NullReferenceException>(task != null);

            lock (TaskCounterlock)
            {
                task.CallCount = ++task.CallCount;
            }
        }

        public bool ResetTaskCounter(int taskId, int currentCount)
        {
            var task = _taskService.LoadTask(taskId);
            task.Counter.Reset(currentCount);
            return true;
        }

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_throttleRules != null)
                {
                    //ThrottleRules.TaskCompletedEvent -= CatchTaskCompletionEvent;
                    //ThrottleRules.TaskUpdatedEvent -= CatchTaskUpdateEvent;
                }

                _throttleRules = null;
                _numberGenerator = null;
            }
        }

        #endregion
    }
}