﻿using System;
using System.Collections.Generic;
using FeatureThrottle.Data.Provider;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Business
{
    public sealed class TaskService : ITaskService
    {
        private static readonly object DataUpdateLock = new object();
        public delegate void ChangedEventHandler(object sender, TaskArgs e);
        public event ChangedEventHandler TaskChangedEvent;
        private readonly ITaskRepository _taskRepository;

        public TaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public void UpdateTask(object sender, TaskArgs e)
        {
            UpdateTaskRecord(e);
        }

        public void CompleteTask(object sender, TaskArgs e)
        {
            lock (DataUpdateLock)
            {
                _taskRepository.UpdateAsComplete(e.TaskId);
            }
        }

        private void OnTaskChanged(TaskArgs e)
        {
            if (TaskChangedEvent != null)
            {
                TaskChangedEvent(this, e);
            }
        }

        public Task LoadTask(int taskId)
        {
            lock (DataUpdateLock)
            {
                var task = _taskRepository.GetById(taskId);

                if (task != null)
                {
                    task.TaskChangedEvent += UpdateTask;
                    task.TaskCompletedEvent += CompleteTask;
                }

                return task;
            }
        }

        public List<Task> LoadTasks
        {
            get
            {
                lock (DataUpdateLock)
                {
                    var tasks = _taskRepository.GetAll();

                    foreach (var task in tasks)
                    {
                        task.TaskChangedEvent += UpdateTask;
                        task.TaskCompletedEvent += CompleteTask;
                    }
                    return tasks;
                }
            }
        }

        private void UpdateTaskRecord(TaskArgs e)
        {
            lock (DataUpdateLock)
            {
                var dbTask = _taskRepository.GetById(e.TaskId);

                if (dbTask == null)
                {
                    throw new NullReferenceException("UpdateTaskRecord:Task cannot be null");
                }

                var task = new Task
                {
                    TaskId = e.TaskId,
                    Counter =
                    {
                        CurrentCount = dbTask.Counter.CurrentCount > e.CurrentTaskCount
                            ? dbTask.Counter.CurrentCount + 1
                            : e.CurrentTaskCount
                    },
                    CallCount = dbTask.CallCount > e.CallCount 
                        ? dbTask.CallCount + 1 
                        : e.CallCount
                };

                _taskRepository.Update(task);

                OnTaskChanged(new TaskArgs
                {
                    CurrentTaskCount = task.Counter.CurrentCount,
                    TaskEndTime = task.ActualEnd,
                    TaskId = task.TaskId,
                    CallCount = task.CallCount
                });
            }
        }
    }
}