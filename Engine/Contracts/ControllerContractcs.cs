﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Contracts
{
    [ContractClassFor(typeof(IController))]
    public abstract class ControllerContractcs : IController
    {
        public IThrottle Throttle { get; set; }

        public List<Task> GetAllActiveTasks()
        {
            Contract.Requires<NullReferenceException>(Throttle != null, "Throttle cannot be NULL.");
            return null;
        }

        public bool StopThrottle()
        {
            Contract.Requires<NullReferenceException>(Throttle != null, "Throttle cannot be NULL.");
            return false;
        }

        public bool StartThrottle()
        {
            Contract.Requires<NullReferenceException>(Throttle != null, "Throttle cannot be NULL.");
            return false;
        }

        public bool ResetTaskCounters()
        {
            Contract.Requires<NullReferenceException>(Throttle != null, "Throttle cannot be NULL.");
            Contract.Requires<NullReferenceException>(Throttle.Tasks != null, "Throttle Task property cannot be NULL.");
            return false;
        }

        public void ResetThrottle()
        {
            Contract.Requires<NullReferenceException>(Throttle != null, "Throttle cannot be NULL.");
        }

        public void CatchTaskCompletionEvent(object sender, TaskArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
