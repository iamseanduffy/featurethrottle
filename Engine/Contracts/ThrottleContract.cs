﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Domain.Interfaces;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Contracts
{
    [ContractClassFor(typeof(IThrottle))]
    public abstract class ThrottleContract : IThrottle 
    {
        public List<Task> Tasks { get; set; }
        public ICount CallCounter { get; set; }
        public bool IsThrottleStopped { get; set; }
        public IThrottleRules ThrottleRules { get; set; }

        public ThrottleResponse GetResponse(ThrottleRequest throttleRequest)
        {
            Contract.Requires<ArgumentNullException>(throttleRequest != null,"ThrottleRequest cannot be Null.");
            return null;
        }

        public bool ResetTaskCounter(int taskId, int currentCount)
        {
            throw new NotImplementedException();
        }

        public Task GetThrottleTaskById(int taskId)
        {
            throw new NotImplementedException();
        }

        public void ResetTaskCounters()
        {
            Contract.Requires<NullReferenceException>(Tasks != null,"The Tasks property cannot be Null");
        }
    }
}
