﻿using System;
using System.Diagnostics.Contracts;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Contracts
{
    [ContractClassFor(typeof(INumberGenerator))]
    public abstract class NumberGeneratorContract :  INumberGenerator
    {
        private const int MinNumber = 0;
        private const int MaxNumber = 100;

        public bool IsProbable(int probability)
        {
            Contract.Requires<ArgumentOutOfRangeException>(probability >= MinNumber, "The probability requested is less that the minimum");
            Contract.Requires<ArgumentOutOfRangeException>(probability <= MaxNumber, "The probability requested is greater than the maximum");
            return false;
        }
    }
}
