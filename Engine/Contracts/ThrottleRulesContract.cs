﻿using System;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Domain.Types;
using FeatureThrottle.Engine.Interfaces;

namespace FeatureThrottle.Engine.Contracts
{
    [ContractClassFor(typeof(IThrottleRules))]
    public abstract class ThrottleRulesContract : IThrottleRules
    {
        public bool ApplyRules(Task task)
        {
            Contract.Requires<ArgumentNullException>(task != null,"Task object cannot be null.");
            return true;
        }

        public event CompletedEventHandler TaskCompletedEvent
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }

        public event UpdatedEventHandler TaskUpdatedEvent
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }
    }
}
