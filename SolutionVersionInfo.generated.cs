using System.Reflection;

[assembly: AssemblyInformationalVersion(@"Pex: commit 657e7f67db0580309b8ba3160621b119a095fb98 from location 'https://git.dfc.local/gerrit/parkerfox/ApplicationThrottle'")]

namespace Pex
{
    internal static class VersionInfo
    {
        public const string CommitIdentifier = @"657e7f67db0580309b8ba3160621b119a095fb98";
    }
}
