﻿namespace FeatureThrottle.Domain.Interfaces
{
    public interface ICount
    {
        void Add();
        void Reset(int currentCount);
        int  CurrentCount { get; set; }
    }    
}
