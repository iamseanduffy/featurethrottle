﻿using FeatureThrottle.Domain.Entities;

namespace FeatureThrottle.Domain.Types
{
    public delegate void CompletedEventHandler(object sender, TaskArgs e);
    public delegate void UpdatedEventHandler(object sender, TaskArgs e);
}
