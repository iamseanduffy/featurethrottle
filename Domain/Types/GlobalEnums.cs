﻿namespace FeatureThrottle.Domain.Types
{
    public enum OutcomeText
    {
        Unknown = 0,
        Accept = 1,
        Decline = 2,
        RulesDecline = 3,
        TaskNotFound = 4,
        ThrottleStopped = 10
    }
}
