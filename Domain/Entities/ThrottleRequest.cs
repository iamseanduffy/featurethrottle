﻿using System.Runtime.Serialization;

namespace FeatureThrottle.Domain.Entities
{
    [DataContract]
    public class ThrottleRequest
    {
        [DataMember]
        public int TaskId { get; set; }
    }
}
