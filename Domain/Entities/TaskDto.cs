﻿using System;

namespace FeatureThrottle.Domain.Entities
{
    public class TaskDto
    {
        public int TaskId { get; set; }
        public string Name { get; set; }
        public DateTime ScheduledStart { get; set; }
        public DateTime ScheduledEnd { get; set; }
        public DateTime ActualEnd { get; set; }
        public int Probability { get; set; }
        public virtual CounterDto Counter { get; set; }
        public virtual int CallCount { get; set; }
    }
}