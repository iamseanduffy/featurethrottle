﻿using System.Runtime.Serialization;
using FeatureThrottle.Domain.Types;

namespace FeatureThrottle.Domain.Entities
{
    [DataContract]
    public class ThrottleResponse
    {
        [DataMember]
        public bool State { get; set; }

        [DataMember]
        public OutcomeText Outcome { get; set; }
    }
}
