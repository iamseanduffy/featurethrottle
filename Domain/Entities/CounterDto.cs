﻿namespace FeatureThrottle.Domain.Entities
{
    public class CounterDto
    {
        public int TargetNumber { get; set; }
        public int CurrentCount { get; set; }
    }
}
