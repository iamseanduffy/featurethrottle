﻿using System;

namespace FeatureThrottle.Domain.Entities
{
    [Serializable]
    public class Task : TaskDto
    {
        public delegate void ChangedEventHandler(object sender, TaskArgs e);

        public delegate void CompletedEventHandler(object sender, TaskArgs e);

        public event ChangedEventHandler TaskChangedEvent;
        public event CompletedEventHandler TaskCompletedEvent;

        public Task()
        {
            Counter = new Counter(this);
            Counter.Changed += CounterChanged;
            CallCount = 0;
        }

        private void CounterChanged(object sender, TaskArgs e)
        {
            OnTaskChanged(new TaskArgs
            {
                CurrentTaskCount = Counter.CurrentCount,
                TaskId = TaskId,
                CallCount = CallCount
            });

            if (Counter.CurrentCount >= Counter.TargetNumber)
            {
                OnTaskCompleted(new TaskArgs
                {
                    CurrentTaskCount = Counter.CurrentCount,
                    TaskEndTime = DateTime.Now,
                    TaskId = TaskId,
                    CallCount = CallCount
                });
            }
        }

        private void OnTaskCompleted(TaskArgs e)
        {
            if (TaskCompletedEvent != null)
            {
                TaskCompletedEvent(this, e);
            }
        }

        private void OnTaskChanged(TaskArgs e)
        {
            if (TaskChangedEvent != null)
            {
                TaskChangedEvent(this, e);
            }
        }

        public new Counter Counter { get; private set; }

        private int _callCounter;

        public override int CallCount
        {
            get { return _callCounter; }
            set
            {
                _callCounter = value;
                OnTaskChanged(new TaskArgs
                {
                    CurrentTaskCount = Counter.CurrentCount,
                    TaskId = TaskId,
                    CallCount = CallCount
                });
            }
        }
    }
}