﻿using System;
using FeatureThrottle.Domain.Interfaces;

namespace FeatureThrottle.Domain.Entities
{
    [Serializable]
    public class Counter : CounterDto, ICount
    {
        #region Delegates

        public event ChangedEventHandler Changed;
        public delegate void ChangedEventHandler(object sender, TaskArgs e);

        #endregion

        private readonly Task _task;

        public Counter(Task task)
        {
            _task = task;
            CurrentCount = 0;
        }

        #region ICount Members

        public void Add()
        {
            CurrentCount = ++CurrentCount;
            OnChanged();
        }

        public void Reset(int currentCount = 0)
        {
            CurrentCount = currentCount;
            OnChanged();
        }

        #endregion        

        protected virtual void OnChanged(TaskArgs e)
        {
            if (Changed != null)
                Changed(this, e);
        }

        private void OnChanged()
        {
            OnChanged(new TaskArgs
            {
                TaskEndTime = _task.ActualEnd,
                CurrentTaskCount = CurrentCount,
                TaskId = _task.TaskId,
                CallCount = _task.CallCount
            });
        }
    }
}