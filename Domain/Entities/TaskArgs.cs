﻿using System;

namespace FeatureThrottle.Domain.Entities
{
    public class TaskArgs : EventArgs
    {
        public int TaskId { get; set; }
        public int CurrentTaskCount { get; set; }
        public int CallCount { get; set; }
        public DateTime TaskEndTime { get; set; }
    }
}
