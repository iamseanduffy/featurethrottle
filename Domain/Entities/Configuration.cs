﻿namespace FeatureThrottle.Domain.Entities
{
    public class Configuration
    {
        public bool StopThrottle { get; set; }        
        public int TaskLoadInterval { get; set; }
    }
}
