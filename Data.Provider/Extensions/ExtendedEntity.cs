﻿using System;
using System.IO;
using EFProviderWrapperToolkit;
using EFTracingProvider;
using FeatureThrottle.Data.Provider.Edmx;

namespace FeatureThrottle.Data.Provider.Extensions
{
    public class ExtendedEntity : ThrottleEntities
    {
        private TextWriter _logOutput;

        public ExtendedEntity()
            : this("name=ThrottleEntities")
        { }
        public ExtendedEntity(string connectionString)
            : base(EntityConnectionWrapperUtils.CreateEntityConnectionWithWrappers(
                connectionString, "EFTracingProvider"))
        { }

        #region Tracing Extensions

        private EFTracingConnection TracingConnection
        {
            get { return this.UnwrapConnection<EFTracingConnection>(); }
        }

        public TextWriter Log
        {
            get { return _logOutput; }
            set
            {
                if ((_logOutput != null) != (value != null))
                {
                    if (value == null)
                        CommandExecuting -= AppendToLog;
                    else
                        CommandExecuting += AppendToLog;
                }

                _logOutput = value;
            }
        }

        public event EventHandler<CommandExecutionEventArgs> CommandExecuting
        {
            add { TracingConnection.CommandExecuting += value; }
            remove { TracingConnection.CommandExecuting -= value; }
        }

        public event EventHandler<CommandExecutionEventArgs> CommandFinished
        {
            add { TracingConnection.CommandFinished += value; }
            remove { TracingConnection.CommandFinished -= value; }
        }

        public event EventHandler<CommandExecutionEventArgs> CommandFailed
        {
            add { TracingConnection.CommandFailed += value; }
            remove { TracingConnection.CommandFailed -= value; }
        }

        private void AppendToLog(object sender, CommandExecutionEventArgs e)
        {
            if (_logOutput == null) return;

            _logOutput.WriteLine(e.ToTraceString().TrimEnd());
            _logOutput.WriteLine();
        }

        #endregion
    }
}
