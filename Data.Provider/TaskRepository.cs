﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using EFTracingProvider;
using FeatureThrottle.Data.Provider.Mappings;
using FeatureThrottle.Domain.Entities;

namespace FeatureThrottle.Data.Provider
{
    public class TaskRepository : DataProviderBase, ITaskRepository
    {
        private static string _sqlAnalyticalData;
        private static void CommandFinished(object sender, CommandExecutionEventArgs e)
        {
            _sqlAnalyticalData += string.Format("Duration {0} | {1}\n\n", e.Duration, e.ToTraceString());
        }

        public void Update(Task data)
        {
            using (var conn = new SqlConnection(SqlConnection))
            {
                const string sql = "update throttle.Task set currentcount = @CurrentCount, callcount = @CallCount where TaskId = @Id";
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = data.TaskId;

                    cmd.Parameters.Add("@CurrentCount", SqlDbType.Int);
                    cmd.Parameters["@CurrentCount"].Value = data.Counter.CurrentCount;

                    cmd.Parameters.Add("@CallCount", SqlDbType.Int);
                    cmd.Parameters["@CallCount"].Value = data.CallCount;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void UpdateAsComplete(int taskId)
        {
            using (var conn = new SqlConnection(SqlConnection))
            {
                const string sql = "update throttle.Task set ActualEnd = GETDATE() where TaskId = @Id";
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = taskId;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<Task> GetAll()
        {
            using (var conn = new SqlConnection(SqlConnection))
            {
                const string sql = "select * from throttle.Task where scheduledEnd > @Now";
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@Now", SqlDbType.DateTime);
                    cmd.Parameters["@Now"].Value = DateTime.Now;

                    conn.Open();
                    var list = new List<Task>();
                    using (var rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            list.Add(new Task
                            {
                                TaskId = (int)rdr[0],
                                Name = (string)rdr[1],
                                ScheduledStart = (DateTime)rdr[2],
                                ScheduledEnd = (DateTime)rdr[3],
                                Counter = { CurrentCount = (int)rdr[7], TargetNumber = (int)rdr[6] },
                                CallCount = (int)rdr[8],
                                Probability = (int)rdr[5]
                            });
                        }
                    }
                    return list;
                }
            }
        }

        public Task GetById(int taskId)
        {
            using (var conn = new SqlConnection(SqlConnection))
            {
                const string sql = "select * from throttle.Task where TaskId = @Id";
                using (var cmd = new SqlCommand(sql, conn))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int);
                    cmd.Parameters["@Id"].Value = taskId;

                    conn.Open();
                    using (var rdr = cmd.ExecuteReader())
                    {
                        while(rdr.Read())
                        {
                            return new Task
                            {
                                TaskId = (int) rdr[0],
                                Name = (string) rdr[1],
                                ScheduledStart = (DateTime) rdr[2],
                                ScheduledEnd = (DateTime) rdr[3],
                                Counter = {CurrentCount = (int) rdr[7], TargetNumber = (int) rdr[6]},
                                CallCount = (int) rdr[8],
                                Probability = (int) rdr[5]
                            };
                        }
                    }
                    return null;
                }
            }
        }

        [Obsolete]
        public static void WriteActiveTask(Task data)
        {
            using (var ctx = ThrottleDataContext)
            {
                using (var scope = CreateTransactionScope())
                {
                    var task = ctx.Task.FirstOrDefault(t => t.TaskId == data.TaskId);
                    Mapper.CounterUpdateMapToEdmx(data, task);

                    ctx.SaveChanges();
                    scope.Complete();

                    //Logger.Log(string.Format("WriteActiveTask \n\n{0}", _sqlAnalyticalData), TraceEventType.Information);
                    //return Mapper.EdmxMapToTask(task);
                }
            }
        }

        [Obsolete]
        public static void WriteCompleteTask(int taskId)
        {
            using (var ctx = ThrottleDataContext)
            {
                //ctx.CommandFinished += EFCo77mmandFinished;
                using (var scope = CreateTransactionScope())
                {
                    //var task = new Edmx.Task { Id = data.Id, ActualEnd = data.ActualEnd };
                    //ctx.Tasks.Attach(task);
                    //ctx.Entry(task).Property(x => x.ActualEnd).IsModified = true;

                    var task = ctx.Task.FirstOrDefault(t => t.TaskId == taskId);

                    task.ActualEnd = DateTime.Now;

                    ctx.SaveChanges();
                    scope.Complete();

                    //Logger.Log(string.Format("WriteActiveTask \n\n{0}", _sqlAnalyticalData), TraceEventType.Information);
                }
            }
        }
    }
}
