﻿using System;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;

namespace FeatureThrottle.Data.Provider.Mappings
{
    internal partial class Mapper
    {
        internal static Edmx.Task CounterUpdateMapToEdmx(Task data, Edmx.Task task)
        {
            Contract.Requires<ArgumentNullException>(data != null, "Data.Task cannot be null or empty.");
            Contract.Requires<ArgumentNullException>(task != null, "Edmx.Task cannot be null or empty.");

            //task = task ?? new Edmx.Task();

            task.CurrentCount = data.Counter.CurrentCount;
            task.CallCount = data.CallCount;

            return task;
        }

        internal static Edmx.Task TaskMapToEdmx(Task data, Edmx.Task task)
        {
            Contract.Requires<ArgumentNullException>(data != null, "Task cannot be null or empty.");

            task = task ?? new Edmx.Task();

           task.TaskId = data.TaskId;
            task.TaskName = data.Name ?? task.TaskName;
            task.ScheduledStart = data.ScheduledStart == DateTime.MinValue ? data.ScheduledStart : task.ScheduledStart;
            task.ScheduledEnd = data.ScheduledEnd == DateTime.MinValue ? data.ScheduledEnd : task.ScheduledEnd;
            task.CurrentCount = data.Counter.CurrentCount;
            task.TargetNumber = task.TargetNumber;
            task.CallCount = data.CallCount;
            task.Probability = data.Probability;
            return task;
        }

        internal static Task EdmxMapToTask(Edmx.Task task)
        {
            return new Task
                       {
                           TaskId = task.TaskId,
                           Name = task.TaskName,
                           ScheduledStart = task.ScheduledStart,
                           ScheduledEnd = task.ScheduledEnd,
                           Counter = {CurrentCount = task.CurrentCount, TargetNumber = task.TargetNumber},
                           CallCount = task.CallCount ?? 0,
                           Probability = task.Probability
                       };
        }

    }
}
