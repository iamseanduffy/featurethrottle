﻿using System;
using System.Configuration;
using System.Transactions;
using FeatureThrottle.Data.Provider.Edmx;

namespace FeatureThrottle.Data.Provider
{
    public abstract class DataProviderBase : IDisposable
    {
        protected static ThrottleEntities ThrottleDataContext
        {
            get { return new ThrottleEntities(); }
        }

        protected static string SqlConnection
        {
            get { return ConfigurationManager.ConnectionStrings["ThrottleServiceConnectionString"].ConnectionString; }
        }

        protected static TransactionScope CreateTransactionScope()
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted,
                Timeout = TimeSpan.MaxValue
            };
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void Dispose()
        {
        }
    }
}