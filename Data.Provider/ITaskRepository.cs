﻿using System.Collections.Generic;
using FeatureThrottle.Domain.Entities;

namespace FeatureThrottle.Data.Provider
{
    public interface ITaskRepository
    {
        void Update(Task data);
        void UpdateAsComplete(int taskId);
        List<Task> GetAll();
        Task GetById(int taskId);
    }
}