﻿using System.ServiceModel;

namespace FeatureThrottle.Service.Exceptions
{
    public class ThrottleFaultException : FaultException 
    {
        public FaultCode Errorcode;
        public string Details;
    }
}