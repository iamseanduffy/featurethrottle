﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Service.Interfaces;

namespace FeatureThrottle.Service.Contracts
{
    [ContractClassFor(typeof(IThrottleService))]
    public abstract class ThrottleServiceContract : IThrottleService
    {
        public ThrottleResponse CallThrottle(ThrottleRequest throttleRequest)
        {
            Contract.Requires<ArgumentNullException>(throttleRequest != null, "The ThrottleRequest Cannot be Null.");
            return null;
        }

        public List<TaskDto> ActiveTasks()
        {
            return null;
        }

        public bool StopService()
        {
            return false;
        }

        public bool StartService()
        {
            return false;
        }

        public bool ResetTaskCounter(int taskId, int counter)
        {
            return false;
        }

        public bool IsThrottleStopped()
        {
            return false;
        }
    }
}