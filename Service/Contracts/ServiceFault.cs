﻿using System;
using System.Collections;
using System.Runtime.Serialization;

namespace FeatureThrottle.Service.Contracts
{
    [DataContract]
    public class ServiceFault
    {
        [DataMember]
        public string MessageText { get; set; }

        [DataMember]
        public IDictionary Data { get; set; }

        [DataMember]
        public Guid Id { get; set; }
    }
}