﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Service.Contracts;

namespace FeatureThrottle.Service.Interfaces
{
    [ServiceContract]
    [ContractClass(typeof(ThrottleServiceContract))]
    [ExceptionShielding("WcfThrottleException")]  
    public interface IThrottleService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        ThrottleResponse CallThrottle(ThrottleRequest throttleRequest);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        List<TaskDto> ActiveTasks();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        bool StopService();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        bool StartService();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        bool ResetTaskCounter(int taskId, int counter);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        bool IsThrottleStopped();
    }
}
