﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Engine.Business;
using FeatureThrottle.Service.Interfaces;

namespace FeatureThrottle.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single, Namespace = "www.seanduffy.info")]
    public class ThrottleService : IThrottleService
    {
        public ThrottleResponse CallThrottle(ThrottleRequest throttleRequest)
        {
            var controller = ControllerFactory.Create();
            return controller.Throttle.GetResponse(throttleRequest);
        }

        public List<TaskDto> ActiveTasks()
        {
            var controller = ControllerFactory.Create();

            return controller.Throttle.Tasks.Select(task =>
                new TaskDto
                {
                    ActualEnd = task.ActualEnd,
                    CallCount = task.CallCount,
                    TaskId = task.TaskId,
                    Name = task.Name,
                    Probability = task.Probability,
                    ScheduledStart = task.ScheduledStart,
                    ScheduledEnd = task.ScheduledStart,
                    Counter = new CounterDto { CurrentCount = task.Counter.CurrentCount, TargetNumber = task.Counter.TargetNumber}
                }).ToList();
        }

        public bool StopService()
        {
            var controller = ControllerFactory.Create();
            return controller.StopThrottle();
        }

        public bool ResetTaskCounter(int taskId, int counter)
        {
            var controller = ControllerFactory.Create();
            return controller.Throttle.ResetTaskCounter(taskId, counter);
        }

        public bool StartService()
        {
            var controller = ControllerFactory.Create();
            return controller.StartThrottle();
        }

        public bool IsThrottleStopped()
        {
            var controller = ControllerFactory.Create();
            return controller.Throttle.IsThrottleStopped;
        }
    }
}