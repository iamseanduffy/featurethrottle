﻿using System.Runtime.Serialization;

namespace FeatureThrottle.Service.Enums
{
    public enum FaultCode
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        IncorrectParameter,
        [EnumMember]
        NullParameter,
    }
}