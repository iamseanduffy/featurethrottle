﻿using System;
using FeatureThrottle.Engine.Business;
using NUnit.Framework;

namespace FeatureThrottle.Engine.Tests
{
    [TestFixture]
    public class ConfigurationFactoryTests
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetValueByKey_EmptyKeyValue_ThrowArgumentNullException()
        {
            // Act
            ConfigurationBuilder.GetValueByKey(string.Empty);
        }

        [Test]
        public void GetValueByKey_RetrieveTaskFilePathValue_ReturnCorrectValue()
        {
            // Arrange
            const string expected = "TaskConfig.xml";

            // Act
            var actual = ConfigurationBuilder.GetValueByKey("TaskFilePath");

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
