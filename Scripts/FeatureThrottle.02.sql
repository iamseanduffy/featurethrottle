BEGIN
SET XACT_ABORT ON;  --First sign of error especially in complex set of actions
                    --this fails the transaction to deny possibility of some actions complete
                    --and others not. This is a session based setting.
 
DECLARE @errorNumber INT,
        @errorMessage NVARCHAR(4000)
  
    SET @errorNumber = 0;
 
BEGIN TRANSACTION PDE_1_2
    BEGIN TRY
  
CREATE TABLE [Throttle].[Task](
    [TaskId] [int] IDENTITY(1,1) NOT NULL,
    [TaskName] [nvarchar](50) NOT NULL,
    [ScheduledStart] [datetime] NOT NULL,
    [ScheduledEnd] [datetime] NOT NULL,
    [ActualEnd] [datetime] NULL,
    [Probability] [int] NOT NULL,
    [TargetNumber] [int] NOT NULL,
    [CurrentCount] [int] NOT NULL,
    [CallCount] [int] NOT NULL
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED
(
    [TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE ROLE ThrottleSystem AUTHORIZATION ThrottleU;

GRANT SELECT ON SCHEMA::[Throttle]
TO [ThrottleSystem]

GRANT UPDATE (ScheduledEnd, ActualEnd, CurrentCount, CallCount) ON [Throttle].[Task]
TO [ThrottleSystem]

CREATE ROLE ThrottleManager AUTHORIZATION ThrottleU;

GRANT SELECT ON SCHEMA::[Throttle]
TO [ThrottleManager]

GRANT UPDATE (Probability) ON [Throttle].[Task]
TO [ThrottleManager]

CREATE LOGIN [ThrottleServiceU] WITH PASSWORD=N'sa@5!rdd', CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF

CREATE USER [ThrottleServiceU] FOR LOGIN [ThrottleServiceU] WITH DEFAULT_SCHEMA=[Throttle]

IF EXISTS (SELECT 'user exists' FROM sys.database_principals WHERE name = 'ThrottleServiceU')
BEGIN
  PRINT 'Modifying user ThrottleServiceU role access'
  
  EXEC sp_addrolemember N'ThrottleSystem', N'ThrottleServiceU'
END
  
    --Should now be able to commit tran if no errors
     COMMIT TRANSACTION PDE_1_2
    END TRY
 BEGIN CATCH
  
 SELECT
    @errorNumber = ERROR_NUMBER(),
    @errorMessage = ERROR_MESSAGE();
     
    PRINT 'Error Number: ' + CAST(@errorNumber AS VARCHAR(12)) + ', Error Message: ' + @errorMessage + '.'
  
    -- Test XACT_STATE for 1 or -1.
    -- XACT_STATE = 0 means there is no transaction and
    -- a COMMIT or ROLLBACK would generate an error.
    -- Test if the transaction is uncommittable.
    IF (XACT_STATE()) = -1
    BEGIN
        PRINT N'The transaction is in an uncommittable state. Rolling back transaction.'
        ROLLBACK TRANSACTION PDE_1_2
        GOTO endRoutine;
    END;
    -- Test if the transaction is active and valid.This may occur if an error happened on SQL server but did not directly affect the transaction and if so the transaction can still be committed
    IF (XACT_STATE()) = 1
    BEGIN
        PRINT N'The transaction is committable. Committing transaction.'
        COMMIT TRANSACTION PDE_1_2
        GOTO endRoutine;
        END;
 END CATCH
 
 endRoutine:
 /** change completed message to suit action so that its something
 meaningful to whoever is running it **/
 PRINT 'End of Actions ...';
END
GO