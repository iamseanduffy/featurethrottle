﻿using System.ServiceModel;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Service.Tests.ThrottleService;
using NUnit.Framework;

namespace FeatureThrottle.Service.Tests
{
    [TestFixture]
    public class ThrottleServiceTests
    {
        private ThrottleServiceClient _client;

        [SetUp]
        public void Setup()
        {
            _client = new ThrottleServiceClient();
        }

        [TearDown]
        public void TearDown()
        {
            _client = null;
        }

        [Test]
        public void ThrottleService_OpenClient_ClientOpen()
        {
            // Arrange
            const CommunicationState expected = CommunicationState.Opened;

            // Act
            _client.Open();

            // Assert
            Assert.AreEqual(expected, _client.State);
        }

        [Test]
        public void CallThrottle_TargetReached_TaskIsActiveSetToFalse()
        {
            // Arrange
            _client.Open();

            // Act
            _client.CallThrottle(new ThrottleRequest {TaskId = 1});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});
            _client.CallThrottle(new ThrottleRequest {TaskId = 3});

            var actual = _client.CallThrottle(new ThrottleRequest {TaskId = 1});

            // Assert
            Assert.AreEqual(true, actual.State);
        }

        [Test]
        public void ThrottleService_CallServiceStatus_ReturnTasks()
        {
            // Arrange
            _client.Open();

            // Act
            var data = _client.ActiveTasks();

            // Assert
            Assert.IsNotEmpty(data);
        }

        [Test]
        public void StartStopTest()
        {
            _client.Open();

            _client.StartService();
            Assert.AreEqual(false, _client.IsThrottleStopped());

            _client.StopService();
            Assert.AreEqual(true, _client.IsThrottleStopped());

            _client.StartService();
            Assert.AreEqual(false, _client.IsThrottleStopped());

            _client.StopService();
            Assert.AreEqual(true, _client.IsThrottleStopped());
        }
    }
}