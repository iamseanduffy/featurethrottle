﻿using System.ServiceModel;
using FeatureThrottle.Domain.Entities;
using FeatureThrottle.Service.Tests.ThrottleService;
using NUnit.Framework;

namespace FeatureThrottle.Service.Tests
{
    [TestFixture]
    public class ThrottleUnitTest
    {
        private readonly ThrottleServiceClient _client;

        public ThrottleUnitTest()
        {
            _client = new ThrottleServiceClient();
        }

        [Test]
        public void ThrottleService_OpenClient_ClientOpen()
        {
            // Arrange
            const CommunicationState expected = CommunicationState.Opened;

            // Act
            _client.Open();

            // Assert
            Assert.AreEqual(expected, _client.State);
        }

        [Test]
        public void CallThrottle_Task5()
        {
            _client.Open();
            // Act
            var actual = _client.CallThrottle(new ThrottleRequest {TaskId = 58});
        }

        [Test]
        public void CallThrottle_Task1()
        {
            // Arrange
            //const ProcessingApplicationType expected = ProcessingApplicationType.Loanbook;
            _client.Open();
            // Act
            var actual = _client.CallThrottle(new ThrottleRequest {TaskId = 6});
        }

        [Test]
        public void CallThrottle_Task3()
        {
            _client.Open();
            // Act
            var actual = _client.CallThrottle(new ThrottleRequest {TaskId = 7});
        }

        [Test]
        public void CallThrottle_Task4()
        {
            _client.Open();
            // Act
            var actual = _client.CallThrottle(new ThrottleRequest {TaskId = 9});
        }
    }
}